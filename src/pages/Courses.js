
import coursesData from './../mockData/courses';
import Course from './../components/Course';

export default function TryCourse(){


	const courses = coursesData.map(course => {

		return <Course key={course.id} countProp={course}/>
	})
	
	return(
		courses
	)
}
