import {useState, useEffect} from 'react'
import {Form, Button, Row, Col, Container} from 'react-bootstrap'


export default function Login(){

	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	// useEffect(function, options)
	useEffect(() => {
		console.log('render')

		// if all fields are filled out and pw & vpw is equal, change the state to false
		if((email !== "" && pw !== "")){

			setIsDisabled(false)

		} else {
			//if all input fields are empty, keep the state of the button to true
			setIsDisabled(true)
		}

		//listen to state changes: fn, ln, em, pw, vf
	}, [email, pw])

	const registerUser = (e) => {
		e.preventDefault()
		alert('Login Successfully')
		// fetch('http://localhost:3007/api/users/email-exists', {
		// 	method: "POST",
		// 	headers: {
		// 		"Content-Type": "application/json"
		// 	},
		// 	body: JSON.stringify({
		// 		email: email
		// 	})
		// })
		// .then(response => response.json())
		// .then(response => {
		// 	// console.log(response)	//false
		// 	if(!response){
		// 		//send request to register
		// 		fetch('http://localhost:3007/api/users/register', {
		// 			method: "POST",
		// 			headers: {
		// 				"Content-Type": "application/json"
		// 			},
		// 			body: JSON.stringify({
		// 				email: email,
		// 				password: pw
		// 			})
		// 		})
		// 		.then(response => response.json())
		// 		.then(response => {
		// 			// console.log(response)

		// 			if(response){
		// 				alert('Registratiion Successful.')

		// 				//redirect
		// 				window.location.href('/login')
		// 			} else
		// 			{
		// 				alert('Something went wrong. Please try again')
		// 			}
		// 		})


		// 	} else{
		// 		alert(`User already exists`)
		// 	}
		// })
	}

	return( 
		<Container className="m-5">
		<h1 className="text-center">Welcome to login page!</h1>
		 	<h3 className="text-center">Login</h3>

			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => registerUser(e) }>

						<Form.Group className="mb-3">
							<Form.Label>Email address</Form.Label>
					    	<Form.Control 
					    		type="email" 
					    		value={email}
					    		onChange={(e) => setEmail(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
					    	<Form.Label>Password</Form.Label>
					    	<Form.Control 
					    		type="password" 
					    		value={pw}
					    		onChange={(e) => setPW(e.target.value)}
					    	/>
						</Form.Group>
						<Button 
							variant="info" 
							type="submit"
							disabled={isDisabled}
						>
							Submit
						</Button>
					</Form>
				</Col>
			</Row>
		</Container>

		)
}
